<?php

namespace Modules\Ticket\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Ticket\Http\Traits\Searchable;

class TicketFrequentlyAskedQuestion extends Model
{
    use HasFactory, Searchable;

    protected $fillable = ['title'];

    protected $search_fields = [
        'title',
    ];

    protected static function newFactory()
    {
        return \Modules\Ticket\Database\factories\TicketFrequentlyAskedQuestionFactory::new();
    }
}
